package com.tim.android.mgdbmobile;

import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ParsersCollection {
	
	private static final String XML_PERSON = "person";
	private static final String XML_STUDENT = "student";
	private static final String XML_ADVISOR = "advisor";
	

	public static void parseSimpleSearchResult(ArrayList<Person> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		
		while(eventType != XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG && XML_PERSON.equals(parser.getName())){
				String id = parser.getAttributeValue(null, "id");
				String name = parser.getAttributeValue(null, "name");
				String univerisy = parser.getAttributeValue(null, "uni");
				String year = parser.getAttributeValue(null, "year");
				
				Person person = new Person();
				
				person.setName(name);
				person.setId(id);
				person.setUnverisity(univerisy);
				person.setYear(year);
				
				items.add(person);
			}
			
			eventType = parser.next();
		}
	}
	
	public static void parseDetailPerson(ArrayList<Person> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		
		while(eventType != XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG && XML_PERSON.equals(parser.getName())){
				String id = parser.getAttributeValue(null, "id");
				String name = parser.getAttributeValue(null, "name");
				String univerisy = parser.getAttributeValue(null, "uni");
				String year = parser.getAttributeValue(null, "year");
				String title = parser.getAttributeValue(null, "title");
				String descendants = parser.getAttributeValue(null, "descendants");
				
				Person person = new Person();
				
				person.setName(name);
				person.setId(id);
				person.setUnverisity(univerisy);
				person.setYear(year);
				person.setDissertation(title);
				person.setDescendants(Integer.parseInt(descendants));
				
				items.add(person);
			}
			
			eventType = parser.next();
		}
	}
	
	public static void parseDetailStudents(ArrayList<Person> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		
		while(eventType != XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG && XML_STUDENT.equals(parser.getName())){
				String id = parser.getAttributeValue(null, "id");
				String name = parser.getAttributeValue(null, "name");
				String univerisy = parser.getAttributeValue(null, "uni");
				String year = parser.getAttributeValue(null, "year");
				String descendants = parser.getAttributeValue(null, "descendants");
				
				Person person = new Person();
				
				person.setName(name);
				person.setId(id);
				person.setUnverisity(univerisy);
				if(year == null)
					year = "0";
				person.setYear(year);
				person.setDescendants(Integer.parseInt(descendants));
				
				items.add(person);
			}
			
			eventType = parser.next();
		}
	}
	
	public static void parseDetailAdvisors(ArrayList<Person> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		
		while(eventType != XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG && XML_ADVISOR.equals(parser.getName())){
				String id = parser.getAttributeValue(null, "id");
				String name = parser.getAttributeValue(null, "name");
				
				Person person = new Person();
				
				person.setName(name);
				person.setId(id);
				
				items.add(person);
			}
			
			eventType = parser.next();
		}
	}
	
	public static void parseLCAResult(ArrayList<Person> items, XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.next();
		
		while(eventType != XmlPullParser.END_DOCUMENT){
			if(eventType == XmlPullParser.START_TAG && XML_PERSON.equals(parser.getName())){
				String id = parser.getAttributeValue(null, "id");
				
				Person person = new Person();

				person.setId(id);
				
				items.add(person);
			}
			
			eventType = parser.next();
		}
	}
}
