package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class PersonDetailFragment extends Fragment {

	public static final String TAG = "com.mgdb.persondetailfragment.persondetail";

	private ArrayList<Person> mPerson;
	private ArrayList<Person> mStudents;
	private ArrayList<Person> mAdvisors;

	ActionMode mMode;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private ArrayAdapter<String> mAdapter;

	private AsyncTask<String, Void, String> mTask;
	private boolean offline;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		setHasOptionsMenu(true);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_detail, parent, false);

		mDrawerLayout = (DrawerLayout) v.findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout,
				R.drawable.ic_drawer, R.string.navigation_drawer_open,
				R.string.navigation_drawer_close);

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		// mDrawerLayout.setBackgroundColor(Color.WHITE);

		mDrawerList = (ListView) v.findViewById(R.id.left_drawer);
		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {

						if (mPerson != null
								&& LCALab.get(getActivity()).getPeople()
										.get(position).getId()
										.equals(mPerson.get(0).getId())) {
							mDrawerLayout.closeDrawers();
							return;
						}

						Intent mIntent = new Intent(getActivity(),
								PersonDetailActivity.class);
						mIntent.putExtra(
								PersonDetailFragment.TAG,
								LCALab.get(getActivity()).getPeople()
										.get(position).getId());

						startActivity(mIntent);
					}
				});

		mDrawerList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		mDrawerList.setMultiChoiceModeListener(new MultiChoiceModeListener() {

			@SuppressWarnings("unchecked")
			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

				mMode = mode;

				switch (item.getItemId()) {
				case R.id.menu_item_remove_lca:
					ArrayAdapter<String> adapter = (ArrayAdapter<String>) mDrawerList
							.getAdapter();
					LCALab lcaLab = LCALab.get(getActivity());
					for (int i = adapter.getCount() - 1; i >= 0; i--) {
						if (mDrawerList.isItemChecked(i))
							lcaLab.removeLCA(adapter.getItem(i));
					}
					mode.finish();
					updateList();
					return true;
				default:
					return false;
				}
			}

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater inflater = mode.getMenuInflater();
				inflater.inflate(R.menu.fragment_detailperson_context, menu);
				return true;
			}

			@Override
			public void onDestroyActionMode(ActionMode arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode mode,
					int position, long id, boolean checked) {

			}

		});

		updateList();

		String id = getActivity().getIntent().getStringExtra(TAG);

		offline = PreferenceManager.getDefaultSharedPreferences(getActivity())
				.getBoolean(SearchFragment.PREF_OFFLINE, false);

		mTask = new FetchDetailTask().execute(id);

		return v;
	}

	@Override
	public void onStop() {
		super.onStop();
		mTask.cancel(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		updateList();
		if (mPerson != null)
			getActivity().invalidateOptionsMenu();
	}

	private void updateList() {
		if (mAdapter == null) {
			mAdapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_list_item_activated_1, LCALab.get(
							getActivity()).getNmaes());

			mDrawerList.setAdapter(mAdapter);
		} else {
			mAdapter.clear();
			mAdapter.addAll(LCALab.get(getActivity()).getNmaes());
			mAdapter.notifyDataSetChanged();
		}

		getActivity().invalidateOptionsMenu();

	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActivity().getActionBar().getThemedContext();
		} else {
			return getActivity();
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {

		MenuItem add = menu.findItem(R.id.menu_item_new_lca);

		if (mPerson != null) {
			if (LCALab.get(getActivity()).contains(mPerson.get(0))) {
				add.setVisible(false);
			} else {
				add.setVisible(true);
			}
		} else {
			add.setVisible(false);
		}

		if (LCALab.get(getActivity()).size() >= 2) {
			menu.findItem(R.id.menu_item_find_lca).setVisible(true);
		} else {
			menu.findItem(R.id.menu_item_find_lca).setVisible(false);
		}

		menu.findItem(R.id.action_offline).setEnabled(
				MGDBDatabaseHelper.exists());

		if (MGDBDatabaseHelper.exists())
			menu.findItem(R.id.action_offline).setChecked(
					PreferenceManager
							.getDefaultSharedPreferences(getActivity())
							.getBoolean(SearchFragment.PREF_OFFLINE, false));
		else {
			PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
					.putBoolean(SearchFragment.PREF_OFFLINE, false).commit();
			menu.findItem(R.id.action_offline).setChecked(false);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_detailperson, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_new_lca:
			LCALab.get(getActivity()).addLCA(mPerson.get(0));
			updateList();
			mDrawerLayout.openDrawer(GravityCompat.END);
			getActivity().invalidateOptionsMenu();
			return true;
		case R.id.menu_item_remove_lca:
			LCALab.get(getActivity()).removeLCA(mPerson.get(0));
			updateList();
			mDrawerLayout.closeDrawers();
			getActivity().invalidateOptionsMenu();
			return true;
		case R.id.menu_item_find_lca:
			Intent i = new Intent(getActivity(), LCADetailActivity.class);
			startActivity(i);
			return true;
		case R.id.action_offline:
			if (!item.isChecked())
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(SearchFragment.PREF_OFFLINE, true)
						.commit();
			else
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(SearchFragment.PREF_OFFLINE, false)
						.commit();

			getActivity().invalidateOptionsMenu();
			return true;
		case android.R.id.home:
			if (NavUtils.getParentActivityName(getActivity()) != null) {
				NavUtils.navigateUpFromSameTask(getActivity());
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void generateView() {

		View v = getView();

		v.findViewById(R.id.detal_progressBar).setVisibility(View.GONE);

		Person person = mPerson.get(0);

		getActivity().invalidateOptionsMenu();

		TextView personName = (TextView) v.findViewById(R.id.detail_name);
		personName.setText(person.getName());

		TextView uni_year = (TextView) v.findViewById(R.id.detail_uni_and_year);
		uni_year.setText(person.getUnverisity() + " " + person.getYear());

		TextView dissertation = (TextView) v
				.findViewById(R.id.detail_dissertation);
		dissertation.setText("Dissertation: " + person.getDissertation());

		// Begin building the advisors fields
		if (mAdvisors.size() > 0) {

			if (mAdvisors.size() == 1) {
				LinearLayout singleAdvisorLayout = new LinearLayout(
						getActivity());
				singleAdvisorLayout.setOrientation(LinearLayout.HORIZONTAL);

				TextView advisor_subtitie = new TextView(getActivity());
				advisor_subtitie.setText("Advisor: ");
				singleAdvisorLayout.addView(advisor_subtitie);

				TextView advisor_name = new TextView(getActivity());

				SpannableString spanString = new SpannableString(mAdvisors.get(
						0).getName());
				spanString.setSpan(new UnderlineSpan(), 0, spanString.length(),
						0);
				advisor_name.setText(spanString);

				advisor_name.setTextColor(Color.parseColor("#0033FF"));

				advisor_name.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(getActivity(),
								PersonDetailActivity.class);
						i.putExtra(PersonDetailFragment.TAG, mAdvisors.get(0)
								.getId());
						startActivity(i);
					}
				});

				singleAdvisorLayout.addView(advisor_name);
				LinearLayout advisorLayout = (LinearLayout) v
						.findViewById(R.id.detail_advisors);
				advisorLayout.addView(singleAdvisorLayout);
			} else {
				int count = 1;
				for (final Person adv : mAdvisors) {
					LinearLayout singleAdvisorLayout = new LinearLayout(
							getActivity());
					singleAdvisorLayout.setOrientation(LinearLayout.HORIZONTAL);

					TextView advisor_subtitie = new TextView(getActivity());
					advisor_subtitie.setText("Advisor " + count + ": ");
					count++;
					singleAdvisorLayout.addView(advisor_subtitie);

					TextView advisor_name = new TextView(getActivity());

					SpannableString spanString = new SpannableString(
							adv.getName());
					spanString.setSpan(new UnderlineSpan(), 0,
							spanString.length(), 0);
					advisor_name.setText(spanString);

					advisor_name.setTextColor(Color.parseColor("#0033FF"));

					advisor_name.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent i = new Intent(getActivity(),
									PersonDetailActivity.class);
							i.putExtra(PersonDetailFragment.TAG, adv.getId());
							startActivity(i);
						}
					});

					singleAdvisorLayout.addView(advisor_name);
					LinearLayout advisorLayout = (LinearLayout) v
							.findViewById(R.id.detail_advisors);
					advisorLayout.addView(singleAdvisorLayout);
				}
			}

		}

		// begin building for students fields
		TextView studentsTitle = (TextView) v
				.findViewById(R.id.detail_student_title);

		if (mStudents.size() > 0) {

			studentsTitle.setText("Students:");

			LinearLayout studentLayout = (LinearLayout) v
					.findViewById(R.id.detail_students);

			LinearLayout studentLayoutRowSub = new LinearLayout(getActivity());
			studentLayoutRowSub.setOrientation(LinearLayout.HORIZONTAL);

			TextView name_sub = new TextView(getActivity());
			name_sub.setText("Name");
			name_sub.setTypeface(null, Typeface.BOLD);
			name_sub.setLayoutParams(new LinearLayout.LayoutParams(0,
					LayoutParams.WRAP_CONTENT, 3f));
			name_sub.setGravity(Gravity.FILL_HORIZONTAL);
			studentLayoutRowSub.addView(name_sub);

			TextView school_sub = new TextView(getActivity());
			school_sub.setText("School");
			school_sub.setTypeface(null, Typeface.BOLD);
			school_sub.setLayoutParams(new LinearLayout.LayoutParams(0,
					LayoutParams.WRAP_CONTENT, 3f));
			studentLayoutRowSub.addView(school_sub);

			TextView year_sub = new TextView(getActivity());
			year_sub.setText("Year");
			year_sub.setTypeface(null, Typeface.BOLD);
			year_sub.setLayoutParams(new LinearLayout.LayoutParams(0,
					LayoutParams.WRAP_CONTENT, 1f));
			year_sub.setGravity(Gravity.CENTER);
			studentLayoutRowSub.addView(year_sub);

			TextView descendants_sub = new TextView(getActivity());
			descendants_sub.setText("Descendants");
			descendants_sub.setTypeface(null, Typeface.BOLD);
			descendants_sub.setLayoutParams(new LinearLayout.LayoutParams(0,
					LayoutParams.WRAP_CONTENT, 2.5f));
			descendants_sub.setGravity(Gravity.CENTER);
			studentLayoutRowSub.addView(descendants_sub);

			studentLayout.addView(studentLayoutRowSub);

			for (final Person stu : mStudents) {
				LinearLayout studentLayoutRow = new LinearLayout(getActivity());
				studentLayoutRow.setOrientation(LinearLayout.HORIZONTAL);

				TextView student_name = new TextView(getActivity());

				SpannableString spanString = new SpannableString(stu.getName());
				spanString.setSpan(new UnderlineSpan(), 0, spanString.length(),
						0);
				student_name.setText(spanString);

				student_name.setTextColor(Color.parseColor("#0033FF"));

				student_name.setLayoutParams(new LinearLayout.LayoutParams(0,
						LayoutParams.WRAP_CONTENT, 3f));

				studentLayoutRow.addView(student_name);

				TextView school = new TextView(getActivity());
				school.setText(stu.getUnverisity());
				school.setLayoutParams(new LinearLayout.LayoutParams(0,
						LayoutParams.WRAP_CONTENT, 3f));
				studentLayoutRow.addView(school);

				TextView year = new TextView(getActivity());
				year.setText(stu.getYear());
				year.setLayoutParams(new LinearLayout.LayoutParams(0,
						LayoutParams.WRAP_CONTENT, 1f));
				year.setGravity(Gravity.CENTER);
				studentLayoutRow.addView(year);

				int descendants = stu.getDescendants();
				TextView descendantText = new TextView(getActivity());
				if (descendants > 0) {
					descendantText.setText(descendants + "");
				} else {
					descendantText.setText("");
				}
				descendantText.setGravity(Gravity.CENTER);
				descendantText.setLayoutParams(new LinearLayout.LayoutParams(0,
						LayoutParams.WRAP_CONTENT, 2.5f));
				studentLayoutRow.addView(descendantText);

				studentLayoutRow.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(getActivity(),
								PersonDetailActivity.class);
						i.putExtra(PersonDetailFragment.TAG, stu.getId());
						startActivity(i);

					}
				});

				studentLayout.addView(studentLayoutRow);
			}

		} else {
			studentsTitle.setText("No students known.");
		}

		// begin building for summary

		if (person.getDescendants() > 0) {
			String summary = getString(R.string.detail_summary, person
					.getName().trim(), mStudents.size() + "",
					person.getDescendants() + "");
			((TextView) v.findViewById(R.id.detail_summary)).setText(summary);
		}

	}

	private class FetchDetailTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... ids) {

			if (offline) {
				mPerson = new MGDBDatabaseHelper().queryDetailPerson(ids[0]);
				mAdvisors = new MGDBDatabaseHelper().queryDetailAdvisor(ids[0]);
				mStudents = new MGDBDatabaseHelper()
						.queryDetailStudents(ids[0]);
				return null;
			} else
				return Fetcher.getDetail(ids[0]);

		}

		@Override
		protected void onPostExecute(String xmlString) {

			if (xmlString != null) {
				mPerson = new Parser().getPersonDetail(xmlString);
				mStudents = new Parser().getStudentsDetail(xmlString);
				mAdvisors = new Parser().getAdvisorDetail(xmlString);
			}

			updateList();
			generateView();

			return;
		}
	}

}
