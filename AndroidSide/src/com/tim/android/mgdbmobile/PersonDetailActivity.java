package com.tim.android.mgdbmobile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class PersonDetailActivity extends FragmentActivity {
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_fragment);
		
		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		
		if (fragment == null){
			fragment = new PersonDetailFragment();
			
			fm.beginTransaction()
				.add(R.id.fragmentContainer, fragment)
				.commit();
		}
		
	}
	
	

}
