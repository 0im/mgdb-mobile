package com.tim.android.mgdbmobile;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class PullService extends IntentService {

	public PullService() {
		super("PollService");
	}

	@Override
	protected void onHandleIntent(Intent arg0) {

		LCADAGBuilder mBuilder = new LCADAGBuilder();

		LCALab.get(getApplicationContext()).setMap(mBuilder.tMap);

		Notification notification = new NotificationCompat.Builder(this)
				.setTicker("Finished Building the DAG")
				.setSmallIcon(android.R.drawable.ic_dialog_info)
				.setContentTitle("Finished Building the DAG")
				.setContentText("You can now go back to do the LCA search")
				.setAutoCancel(true).build();
		
		LCALab.get(this).setIsDagBuilt(true);

		NotificationManager notificationManager = (NotificationManager) getApplicationContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(0, notification);

	}

}
