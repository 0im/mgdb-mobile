package com.tim.android.mgdbmobile;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

public class Parser {

	private static final String TAG = "Parser";

	public ArrayList<Person> downloadPeopleInfo(String xmlString, int mode) {

		ArrayList<Person> items = new ArrayList<Person>();

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlString));

			switch (mode) {
			case 0:
				ParsersCollection.parseSimpleSearchResult(items, parser);
				break;
			case 1:
				ParsersCollection.parseDetailPerson(items, parser);
				break;
			case 2:
				ParsersCollection.parseDetailStudents(items, parser);
				break;
			case 3:
				ParsersCollection.parseDetailAdvisors(items, parser);
				break;
			case 4:
				ParsersCollection.parseLCAResult(items, parser);
				break;
			}

		} catch (IOException e) {
			Log.e(TAG, "Failed to parse items", e);
		} catch (XmlPullParserException xppe) {
			Log.e(TAG, "Failed to parse items", xppe);
		}

		return items;
	}

	public ArrayList<Person> getSimpleSearchResult(String xmlString) {

		return downloadPeopleInfo(xmlString, 0);
	}

	public ArrayList<Person> getPersonDetail(String xmlString) {

		return downloadPeopleInfo(xmlString, 1);
	}

	public ArrayList<Person> getStudentsDetail(String xmlString) {

		return downloadPeopleInfo(xmlString, 2);
	}

	public ArrayList<Person> getAdvisorDetail(String xmlString) {

		return downloadPeopleInfo(xmlString, 3);
	}

	public ArrayList<Person> getLCAResult(String xmlString) {

		return downloadPeopleInfo(xmlString, 4);
	}
}
