package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class QuickSearchResultFragment extends ListFragment {

	public static final String TAG = "com.mgdb.qsfragment";

	private ArrayList<Person> mPeople;

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPeople = (ArrayList<Person>) getArguments().getSerializable(TAG);

		ResultAdapter adapter = new ResultAdapter(mPeople);

		setListAdapter(adapter);
		setHasOptionsMenu(true);
		setRetainInstance(true);

	}
	
	@Override
	public void onResume() {
		super.onResume();
		getActivity().invalidateOptionsMenu();
	}

	public static QuickSearchResultFragment newInstance(ArrayList<Person> people) {
		Bundle args = new Bundle();
		args.putSerializable(TAG, people);

		QuickSearchResultFragment fragment = new QuickSearchResultFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		Intent i = new Intent(getActivity(), PersonDetailActivity.class);
		i.putExtra(PersonDetailFragment.TAG, mPeople.get(position).getId());
		startActivity(i);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		inflater.inflate(R.menu.activity_search, menu);
		
		menu.findItem(R.id.action_settings).setVisible(false);

		menu.findItem(R.id.action_offline).setEnabled(
				MGDBDatabaseHelper.exists());
		if (MGDBDatabaseHelper.exists())
			menu.findItem(R.id.action_offline).setChecked(
					PreferenceManager
							.getDefaultSharedPreferences(getActivity())
							.getBoolean(SearchFragment.PREF_OFFLINE, false));
		else {
			PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
					.putBoolean(SearchFragment.PREF_OFFLINE, false).commit();
			menu.findItem(R.id.action_offline).setChecked(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			MGDBDatabaseHelper.downloadDatabase(getActivity());
			return true;
		case R.id.action_offline:
			if (!item.isChecked())
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(SearchFragment.PREF_OFFLINE, true)
						.commit();
			else
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(SearchFragment.PREF_OFFLINE, false)
						.commit();

			getActivity().invalidateOptionsMenu();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class ResultAdapter extends ArrayAdapter<Person> {

		public ResultAdapter(ArrayList<Person> people) {
			super(getActivity(), 0, people);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.list_quick_result, null);
			}

			Person p = getItem(position);

			TextView name = (TextView) convertView.findViewById(R.id.name);
			name.setText(p.getName());

			TextView uni = (TextView) convertView.findViewById(R.id.uni);
			if (p.getUnverisity() != null || !p.getUnverisity().isEmpty())
				uni.setText(p.getUnverisity());

			TextView year = (TextView) convertView.findViewById(R.id.year);

			if (p.getYear() != null) {
				year.setText(p.getYear());
			}

			return convertView;
		}
	}

}
