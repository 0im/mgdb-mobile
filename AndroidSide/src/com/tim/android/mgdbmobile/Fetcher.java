package com.tim.android.mgdbmobile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.net.Uri;
import android.util.Log;

public class Fetcher {

	private static final String TAG = "Fetcher";

	private static final String SEARCH = "http://dev.shinra-co.net/MGDB/workspace/index.php";
	private static final String DETAIL = "http://dev.shinra-co.net/MGDB/workspace/detail.php";
	private static final String LCA = "http://dev.shinra-co.net/MGDB/workspace/lca.php";

	static byte[] getUrlBytes(String urlSpec) throws IOException {
		URL url = new URL(urlSpec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			InputStream in = connection.getInputStream();

			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return null;
			}

			int bytesRead = 0;
			byte[] buffer = new byte[1024];
			while ((bytesRead = in.read(buffer)) > 0) {
				out.write(buffer, 0, bytesRead);
			}
			out.close();
			return out.toByteArray();
		} finally {
			connection.disconnect();
		}
	}

	static String getUrl(String urlSpec) throws IOException {
		return new String(getUrlBytes(urlSpec));
	}

	public static String getDetail(String id) {

		String url = Uri.parse(DETAIL).buildUpon()
				.appendQueryParameter("id", id).build().toString();

		try {
			return getUrl(url);
		} catch (IOException e) {
			Log.e(TAG, "Failed to fetch items", e);
			return null;
		}
	}

	public static String getQuickSearch(String keyword) {

		String url = Uri.parse(SEARCH).buildUpon()
				.appendQueryParameter("method", "0")
				.appendQueryParameter("keyword", keyword).build().toString();

		try {
			return getUrl(url);
		} catch (IOException e) {
			Log.e(TAG, "Failed to fetch items", e);
			return null;
		}
	}

	public static String getLCAId(ArrayList<String> ids) {
		String toSend = "";

		for (String s : ids) {
			toSend = s + " " + toSend;
		}

		String url = Uri.parse(LCA).buildUpon()
				.appendQueryParameter("ids", toSend).build().toString();

		try {
			return getUrl(url);
		} catch (IOException e) {
			Log.e(TAG, "Failed to fetch items", e);
			return null;
		}
	}

}
