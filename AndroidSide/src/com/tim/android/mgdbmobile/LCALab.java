package com.tim.android.mgdbmobile;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;

public class LCALab {

	private ArrayList<Person> mPeople;

	private StringBuffer mAncestor;

	private SparseArray<DAGNode> mMap;

	private static LCALab sLCALab;

	private Context mContext;

	private boolean mIsDagBuilt;
	private boolean mIsDagBuilding;

	private LCALab(Context appContext) {
		mContext = appContext.getApplicationContext();

		mPeople = new ArrayList<Person>();

		mIsDagBuilt = false;
	}

	public void addLCA(Person p) {
		mPeople.add(p);
	}

	public void removeLCA(Person p) {
		for (Person s : mPeople) {
			if (p.getId().equals(s.getId())) {
				mPeople.remove(s);
				return;
			}
		}
	}
	
	public void removeLCA(String p) {
		for (Person s : mPeople) {
			if (p.equals(s.getName())) {
				mPeople.remove(s);
				return;
			}
		}
	}

	public static LCALab get(Context c) {
		if (sLCALab == null) {
			sLCALab = new LCALab(c.getApplicationContext());
		}

		return sLCALab;
	}

	public ArrayList<Person> getPeople() {
		return mPeople;
	}

	public int size() {
		return mPeople.size();
	}

	public ArrayList<String> getNmaes() {
		ArrayList<String> names = new ArrayList<String>();

		for (Person s : mPeople)
			names.add(s.getName());

		return names;
	}

	public ArrayList<String> getIds() {
		ArrayList<String> ids = new ArrayList<String>();

		for (Person s : mPeople)
			ids.add(s.getId());

		return ids;
	}

	public boolean contains(Person p) {
		for (Person s : mPeople) {
			if (p.getId().equals(s.getId()))
				return true;
		}

		return false;
	}

	public ArrayList<Integer> findLCA() {
		mAncestor = new StringBuffer();

		if (mMap == null)
			return null;

		ArrayList<Integer> ids = new ArrayList<Integer>();

		for (String s : getIds())
			ids.add(Integer.parseInt(s));
		
		ArrayList<Integer> anc;

		anc = naiveAlgorithm(ids);
		

		return anc;
	}

	public void setMap(SparseArray<DAGNode> map) {
		mMap = map;
	}

	private ArrayList<Integer> naiveAlgorithm(List<Integer> ids) {

		// Recursively to retrieve all the ancestors for each input id:
		ArrayList<List<Integer>> ancestorList = new ArrayList<List<Integer>>();
		for (int i : ids) {
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			ArrayList<Integer> arr = new ArrayList<Integer>();
			tmp.add(i);
			arr.add(i);
			while (!tmp.isEmpty()) {
				DAGNode node = mMap.get(tmp.get(0));
				DAGNode[] aids = node.getParent();
				if (aids != null && aids.length > 0) {
					for (DAGNode nd : aids) {
						if (nd != null && !arr.contains(nd.getPid())) {
							tmp.add(nd.getPid());
							arr.add(nd.getPid());
						}
					}
				}
				tmp.remove(0);
			}

			ancestorList.add(arr);
		}

		ArrayList<Integer> res = new ArrayList<Integer>();

		for (List<Integer> s : ancestorList) {
			List<Integer> temp = new ArrayList<Integer>();
			for (int i : s) {
				temp.add(i);
			}
		}

		// find the CAs from these ancestors lists:
		List<Integer> tmp = findCommon(ancestorList);

		// For each CA, check if none of its children is in CAs list, then it's
		// LCA
		for (int i : tmp) {
			DAGNode node = mMap.get(i);
			DAGNode[] cids = node.getChildren();
			boolean isCA = false;
			for (DAGNode child : cids) {
				if (tmp.contains(child.getPid())) {
					isCA = true;
					break;
				}
			}
			if (!isCA)
				res.add(i);
		}
		return res;
	}

	/**
	 * Return the intersected int list from a list of List<Integer>
	 * 
	 * @param ArrayList
	 *            of ArrayList<Integer>
	 * @return the intersected integer list
	 */
	private List<Integer> findCommon(ArrayList<List<Integer>> arrList) {
		List<Integer> list = arrList.get(0);
		for (int i = 1; i < arrList.size(); i++) {
			List<Integer> tmp = arrList.get(i);
			list.retainAll(tmp);
		}
		return list;
	}

	public void setIsDagBuilt(boolean isDagBuilt) {
		mIsDagBuilt = isDagBuilt;
	}

	public boolean isIsDagBuilt() {
		return mIsDagBuilt;
	}

	public boolean isIsDagBuilding() {

		if (mIsDagBuilt)
			return false;

		return mIsDagBuilding;
	}

	public void setIsDagBuilding(boolean isDagBuilding) {
		mIsDagBuilding = isDagBuilding;
	}

	public StringBuffer getAncestor() {
		return mAncestor;
	}

}
