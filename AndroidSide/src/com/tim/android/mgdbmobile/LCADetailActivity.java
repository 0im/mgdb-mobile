package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LCADetailActivity extends FragmentActivity {

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*
		 * setContentView(R.layout.activity_fragment);
		 * 
		 * FragmentManager fm = getSupportFragmentManager(); Fragment fragment =
		 * fm.findFragmentById(R.id.fragmentContainer);
		 * 
		 * if (fragment == null){ fragment = new LCADetailFragment();
		 * 
		 * fm.beginTransaction() .add(R.id.fragmentContainer, fragment)
		 * .commit(); }
		 */

		setContentView(R.layout.fragment_lca_detail);

		LinearLayout names = (LinearLayout) findViewById(R.id.fragment_lca_detail_names);

		int i = 0;
		for (Person p : LCALab.get(this).getPeople()) {
			TextView name = new TextView(this);
			name.setText(p.getName());
			name.setTypeface(null, Typeface.BOLD);
			name.setTextSize(18);
			name.setGravity(Gravity.CENTER);

			names.addView(name);

			i++;

			if (i != LCALab.get(this).size()) {
				TextView and = new TextView(this);
				and.setText("And");
				and.setGravity(Gravity.CENTER);
				names.addView(and);
			}
		}

		TextView isare = new TextView(this);
		isare.setText("is / are");
		isare.setGravity(Gravity.CENTER);
		names.addView(isare);

		if (!LCALab.get(this).isIsDagBuilt()) {

			FragmentManager fm = this.getSupportFragmentManager();
			
			LCADAGAlertFragment dialog;
			
			if(LCALab.get(this).isIsDagBuilding())
				dialog = LCADAGAlertFragment.newInstance(1);
			else
				dialog = LCADAGAlertFragment.newInstance(0);

			dialog.show(fm, "LCADAG");

		} else {

			ArrayList<Integer> result = LCALab.get(this).findLCA();

			new FetchItemsTask().execute(result);
		}
	}

	public void doPositiveClick() {
		
		LCALab.get(this).setIsDagBuilding(true);
		
		Intent mServiceIntent = new Intent(this, PullService.class);

		this.startService(mServiceIntent);
		
		finish();
	}

	public void doNegativeClick() {
		finish();
	}

	private class FetchItemsTask extends
			AsyncTask<ArrayList<Integer>, Void, ArrayList<Person>> {

		@Override
		protected ArrayList<Person> doInBackground(
				ArrayList<Integer>... keywords) {

			return new MGDBDatabaseHelper().queryQuickSearch(keywords[0]);

		}

		@Override
		protected void onPostExecute(ArrayList<Person> items) {

			findViewById(R.id.fragment_lca_progressBar)
					.setVisibility(View.GONE);

			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = fm.findFragmentById(R.id.ResultContainer);

			if (fragment == null) {
				fragment = QuickSearchResultFragment.newInstance(items);

				fm.beginTransaction().add(R.id.ResultContainer, fragment)
						.commit();
			}

			return;
		}
	}
}
