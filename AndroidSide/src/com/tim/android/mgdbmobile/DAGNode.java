package com.tim.android.mgdbmobile;

import java.io.Serializable;

public class DAGNode implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5703532157350463056L;
	
	String code;
	int pid; // node id
	DAGNode[] parent; // parent nodes list
	DAGNode[] children; // children nodes list
	String name;

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public DAGNode[] getParent() {
		return parent;
	}

	public void setParent(DAGNode[] parent) {
		this.parent = parent;
	}

	public DAGNode[] getChildren() {
		return children;
	}

	public void setChildren(DAGNode[] children) {
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
