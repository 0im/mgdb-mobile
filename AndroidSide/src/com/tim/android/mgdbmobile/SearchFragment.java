package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.app.ActivityManager;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

public class SearchFragment extends Fragment {

	public static final String PREF_OFFLINE = "OfflineMode";

	private AutoCompleteTextView simpleSearchTextView;
	// private ArrayList<String> mNames;
	private boolean mBlockCompletion;

	private ArrayAdapter<String> mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);
		
		

		setHasOptionsMenu(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().invalidateOptionsMenu();
		getActivity().registerReceiver(DLComplete,
				new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().unregisterReceiver(DLComplete);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_quick_search,
				container, false);

		simpleSearchTextView = (AutoCompleteTextView) rootView
				.findViewById(R.id.quicksearch_autocomplete);
		
		simpleSearchTextView.setAdapter(mAdapter);

		/*
		 * simpleSearchTextView .setOnItemClickListener(new
		 * AdapterView.OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { Intent i = new Intent(getActivity(),
		 * QuickSearchResultActivity.class);
		 * i.putExtra(QuickSearchResultActivity.TAG, mNames.get(position));
		 * startActivity(i);
		 * 
		 * } });
		 */

		mBlockCompletion = false;
		simpleSearchTextView.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// Do nothing

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// Do nothing
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int count,
					int after) {
				if (MGDBDatabaseHelper.exists())
					new FetchSuggestionTask().execute(s.toString());

				if (!mBlockCompletion && start < s.length()
						&& s.charAt(start) == '\n') {
					if (s.length() <= 1) {
						mBlockCompletion = true;
						simpleSearchTextView.setText("");
						mBlockCompletion = false;
						Toast.makeText(getActivity(),
								"Please Enter Something!", Toast.LENGTH_SHORT)
								.show();
					}

					else {
						String toSend = s.subSequence(0, start).toString()
								+ s.subSequence(start + 1, s.length());
						mBlockCompletion = true;
						simpleSearchTextView.setText(toSend);
						simpleSearchTextView.setSelection(s.length() - 1);
						mBlockCompletion = false;
						Intent i = new Intent(getActivity(),
								QuickSearchResultActivity.class);
						i.putExtra(QuickSearchResultActivity.TAG, toSend);
						startActivity(i);
					}
				}
			}

		});

		Button mButton = (Button) rootView
				.findViewById(R.id.quicksearch_button);
		mButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String toSend = simpleSearchTextView.getText().toString();

				Intent i = new Intent(getActivity(),
						QuickSearchResultActivity.class);
				i.putExtra(QuickSearchResultActivity.TAG, toSend);
				startActivity(i);
			}
		});

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		inflater.inflate(R.menu.activity_search, menu);

		menu.findItem(R.id.action_offline).setEnabled(
				MGDBDatabaseHelper.exists());
		if (MGDBDatabaseHelper.exists())
			menu.findItem(R.id.action_offline).setChecked(
					PreferenceManager
							.getDefaultSharedPreferences(getActivity())
							.getBoolean(PREF_OFFLINE, false));
		else {
			PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
					.putBoolean(PREF_OFFLINE, false).commit();
			menu.findItem(R.id.action_offline).setChecked(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			MGDBDatabaseHelper.downloadDatabase(getActivity());
			return true;
		case R.id.action_offline:
			if (!item.isChecked())
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(PREF_OFFLINE, true).commit();
			else
				PreferenceManager.getDefaultSharedPreferences(getActivity())
						.edit().putBoolean(PREF_OFFLINE, false).commit();

			getActivity().invalidateOptionsMenu();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private BroadcastReceiver DLComplete = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			getActivity().invalidateOptionsMenu();
		}

	};

	private class FetchSuggestionTask extends
			AsyncTask<String, Void, ArrayList<String>> {

		@Override
		protected ArrayList<String> doInBackground(String... keywords) {

			return new MGDBDatabaseHelper().queryNameSuggestions(keywords[0]);

		}

		@Override
		protected void onPostExecute(ArrayList<String> items) {
			mAdapter.clear();
			mAdapter.addAll(items);
			//mAdapter.notifyDataSetChanged();

			return;
		}
	}
}
