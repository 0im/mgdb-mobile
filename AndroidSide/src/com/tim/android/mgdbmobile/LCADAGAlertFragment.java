package com.tim.android.mgdbmobile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class LCADAGAlertFragment extends DialogFragment {

	public static LCADAGAlertFragment newInstance(int mode) {
		LCADAGAlertFragment frag = new LCADAGAlertFragment();

		Bundle args = new Bundle();
		args.putInt("mode", mode);
		frag.setArguments(args);
		return frag;

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int mode = getArguments().getInt("mode");

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
				.setIcon(android.R.drawable.ic_menu_close_clear_cancel).setNegativeButton(
						"Cancel", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((LCADetailActivity) getActivity())
										.doNegativeClick();

							}
						});

		if (mode == 0) {

			return builder
					.setTitle("The DAG is not built!")
					.setMessage(
							"In order to use offline LCA function, the DAG must be built first. It may take as long as 5 minutes. After the DAG is built, a notification will be shown. Would you like to proceed?")
					.setPositiveButton("Proceed",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									((LCADetailActivity) getActivity())
											.doPositiveClick();

								}
							}).create();

		} else {

			return builder.setTitle("The DAG is building!")
					.setMessage("Please wait for it to complete.").create();
		}

	}

}
