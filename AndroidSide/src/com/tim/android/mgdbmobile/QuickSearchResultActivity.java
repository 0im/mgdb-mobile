package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

public class QuickSearchResultActivity extends FragmentActivity {

	public static final String TAG = "com.mgdb.android.quickserachresultactivity";

	private TextView mResultText;
	private boolean offline;
	private AsyncTask<String, Void, ArrayList<Person>> mTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quicksearch_result);
		String keyword = getIntent().getStringExtra(TAG);

		offline = PreferenceManager.getDefaultSharedPreferences(this)
				.getBoolean(SearchFragment.PREF_OFFLINE, false);

		mTask = new FetchItemsTask().execute(keyword);
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		mTask.cancel(true);
	}

	private class FetchItemsTask extends
			AsyncTask<String, Void, ArrayList<Person>> {

		@Override
		protected ArrayList<Person> doInBackground(String... keywords) {

			if (offline)
				return new MGDBDatabaseHelper().queryQuickSearch(keywords[0]);
			else
				return new Parser().getSimpleSearchResult(Fetcher
						.getQuickSearch(keywords[0]));

		}

		@Override
		protected void onPostExecute(ArrayList<Person> items) {
			
			findViewById(R.id.result_progressBar).setVisibility(View.GONE);
			
			mResultText = (TextView) findViewById(R.id.result_records);

			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = fm.findFragmentById(R.id.ResultContainer);

			if (fragment == null) {
				fragment = QuickSearchResultFragment.newInstance(items);

				fm.beginTransaction().add(R.id.ResultContainer, fragment).commit();
			}

			String result = getString(R.string.result_records, items.size() + "");
			mResultText.setText(result);

			return;
		}
	}
}
