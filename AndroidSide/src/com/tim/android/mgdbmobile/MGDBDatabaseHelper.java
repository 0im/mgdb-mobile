package com.tim.android.mgdbmobile;

import java.io.File;
import java.util.ArrayList;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;

public class MGDBDatabaseHelper {

	private static final String COLUMN_ID = "pID";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_DES = "onlineDescendants";
	private static final String COLUMN_UNI = "university";
	private static final String COLUMN_YEAR = "year";
	private static final String COLUMN_TITLE = "title";

	private SQLiteDatabase mDatabase;

	public MGDBDatabaseHelper() {

		File path = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File file = new File(path, "MGDB.sqlite");

		String mPath = file.getPath();

		mDatabase = SQLiteDatabase.openDatabase(mPath, null,
				SQLiteDatabase.OPEN_READWRITE);
	}

	public static boolean exists() {
		File path = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

		File file = new File(path, "MGDB.sqlite");

		return file.exists();
	}

	public static void downloadDatabase(Context mContext) {

		String url = "http://dev.shinra-co.net/MGDB/workspace/Admin/lib/db/MGDB";
		DownloadManager.Request request = new DownloadManager.Request(
				Uri.parse(url));
		request.setTitle("Downloading the database");

		request.allowScanningByMediaScanner();
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

		request.setDestinationInExternalPublicDir(
				Environment.DIRECTORY_DOWNLOADS, "MGDB.sqlite");

		// get download service and enqueue file
		DownloadManager manager = (DownloadManager) mContext
				.getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);
	}

	public ArrayList<String> queryNameSuggestions(String keyword) {
		ArrayList<String> people = new ArrayList<String>();

		Cursor wrapped = mDatabase.rawQuery("SELECT name " + "FROM person "
				+ "WHERE name like '%" + keyword + "%'" + " LIMIT 15", null);

		CursorWrapper pc = new CursorWrapper(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			String name = pc.getString(pc.getColumnIndex(COLUMN_NAME));
			people.add(name);
			pc.moveToNext();
		}

		pc.close();

		mDatabase.close();

		return people;
	}

	public ArrayList<Person> queryQuickSearch(String keyword) {
		ArrayList<Person> people = new ArrayList<Person>();

		Cursor wrapped = mDatabase
				.rawQuery(
						"SELECT * "
								+ "FROM person JOIN dissertation ON (person.pID = dissertation.author) "
								+ "WHERE name like '%" + keyword + "%'", null);

		PersonCursor pc = new PersonCursor(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			Person mPerson = pc.getPerson();
			people.add(mPerson);
			pc.moveToNext();
		}

		pc.close();

		mDatabase.close();

		return people;
	}

	public ArrayList<Person> queryQuickSearch(ArrayList<Integer> ids) {
		ArrayList<Person> people = new ArrayList<Person>();

		for (int id : ids) {

			Cursor wrapped = mDatabase
					.rawQuery(
							"SELECT * "
									+ "FROM person JOIN dissertation ON (person.pID = dissertation.author) "
									+ "WHERE pID = " + id, null);

			PersonCursor pc = new PersonCursor(wrapped);

			pc.moveToFirst();

			Person mPerson = pc.getPerson();
			people.add(mPerson);

			pc.close();
		}

		mDatabase.close();

		return people;
	}

	public ArrayList<Person> queryDetailPerson(String id) {
		ArrayList<Person> people = new ArrayList<Person>();

		Cursor wrapped = mDatabase
				.rawQuery(
						"SELECT * "
								+ "FROM person JOIN dissertation ON (person.pID = dissertation.author) "
								+ "WHERE pID = " + id, null);

		PersonCursor pc = new PersonCursor(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			Person mPerson = pc.getPerson();
			people.add(mPerson);
			pc.moveToNext();
		}

		pc.close();

		mDatabase.close();

		return people;
	}

	public ArrayList<Person> queryDetailAdvisor(String id) {
		ArrayList<Person> people = new ArrayList<Person>();

		Cursor wrapped = mDatabase
				.rawQuery(
						"SELECT  pId, name "
								+ "FROM person WHERE person.pID IN "
								+ "(SELECT advisor FROM dissertation JOIN advised ON (advised.student = dissertation.dID) "
								+ "WHERE author =" + id + ")", null);

		PersonCursor pc = new PersonCursor(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			Person mPerson = pc.getPerson();
			people.add(mPerson);
			pc.moveToNext();
		}

		pc.close();

		mDatabase.close();

		return people;
	}

	public ArrayList<Person> queryDetailStudents(String id) {
		ArrayList<Person> people = new ArrayList<Person>();

		Cursor wrapped = mDatabase
				.rawQuery(
						"SELECT * "
								+ "FROM person JOIN dissertation ON (person.pID = dissertation.author) "
								+ "WHERE person.pID IN "
								+ "(SELECT author FROM dissertation JOIN advised on (advised.student = dissertation.dID) "
								+ "WHERE advised.advisor = " + id
								+ ") ORDER BY year", null);

		PersonCursor pc = new PersonCursor(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			Person mPerson = pc.getPerson();
			people.add(mPerson);
			pc.moveToNext();
		}

		pc.close();

		mDatabase.close();

		return people;
	}

	public class PersonCursor extends CursorWrapper {

		public PersonCursor(Cursor c) {
			super(c);
		}

		/**
		 * Returns a Person object configured for the current row, or null if
		 * the current row is invalid.
		 */
		public Person getPerson() {
			if (isBeforeFirst() || isAfterLast())
				return null;
			Person person = new Person();
			person.setId(getString(getColumnIndex(COLUMN_ID)));

			person.setName(getString(getColumnIndex(COLUMN_NAME)));

			int indexUni = getColumnIndex(COLUMN_UNI);
			if (indexUni == -1 || isNull(indexUni))
				person.setUnverisity("");
			else
				person.setUnverisity(getString(indexUni));

			int indexYear = getColumnIndex(COLUMN_YEAR);
			if (indexUni == -1 || isNull(indexYear))
				person.setYear("");
			else
				person.setYear(getString(indexYear));

			int indexDes = getColumnIndex(COLUMN_DES);
			if (indexUni == -1 || isNull(indexDes))
				person.setDescendants(0);
			else
				person.setDescendants(getInt(indexDes));

			int indexDis = getColumnIndex(COLUMN_TITLE);
			if (indexUni == -1 || isNull(indexDis))
				person.setDissertation("");
			else
				person.setDissertation(getString(indexDis));

			return person;
		}
	}
}
