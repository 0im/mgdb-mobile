package com.tim.android.mgdbmobile;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class SearchActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, new SearchFragment()).commit();
	}
	

}
