package com.tim.android.mgdbmobile;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.SparseArray;

public class LCADAGBuilder {

	private static final String COLUMN_ID = "pid";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_STUDENTS = "students";
	private static final String COLUMN_PARENTS = "parents";

	private SQLiteDatabase mDatabase;

	public SparseArray<PersonForLCA> pMap = new SparseArray<PersonForLCA>(); // store
	// the
	// person
	public SparseArray<DAGNode> tMap = new SparseArray<DAGNode>(); // store
																	// the
																	// nodes

	public LCADAGBuilder() {

		File path = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File file = new File(path, "MGDB2.sqlite");

		String mPath = file.getPath();

		mDatabase = SQLiteDatabase.openDatabase(mPath, null,
				SQLiteDatabase.OPEN_READWRITE);

		loadPersonInfo();
		buildDAG();

		pMap.clear();

		return;
	}

	// load the data from db:
	private void loadPersonInfo() {

		/*
		 * Cursor wrapped = mDatabase .rawQuery(
		 * "select pid, name, group_concat(distinct diss.author) as students, group_concat(distinct diss2.advisor) as parents "
		 * + "from person p " +
		 * "left join (select advisor,author from dissertation d, advised a where d.dID = a.student) as diss "
		 * + "on diss.advisor = p.pid " +
		 * "left join (select advisor, author from dissertation d, advised a where d.dID =  a.student) as diss2 "
		 * + "on diss2.author = p.pid " + "group by pid", null);
		 */

		Cursor wrapped = mDatabase.rawQuery("select * from lca", null);

		PersonForLCACursor pc = new PersonForLCACursor(wrapped);

		pc.moveToFirst();

		while (!pc.isAfterLast()) {
			// load the person info:

			PersonForLCA mPerson = pc.getPerson();

			int id = mPerson.getID();
			String name = mPerson.getName();

			this.pMap.put(id, mPerson);

			// create the node from the person info:
			DAGNode node = new DAGNode();
			node.setPid(id);
			node.setName(name);
			this.tMap.put(id, node);

			pc.moveToNext();
		}

		pc.close();
		mDatabase.close();

	}

	// Setting parents and children relationship for each nodes to build the
	// DAG:
	public void buildDAG() {
		for (int k = 0; k < this.pMap.size(); k++) {
			int key = this.pMap.keyAt(k);
			PersonForLCA eachPer = this.pMap.get(key);
			DAGNode eachNode = this.tMap.get(key);

			// Set the parents info for each node:
			List<String> aids = eachPer.getAdvisorsIDs();
			if (aids != null && aids.size() > 0) {
				DAGNode[] advisors = new DAGNode[aids.size()];
				for (int i = 0; i < aids.size(); i++) {
					DAGNode tmp = this.tMap.get(Integer.valueOf(aids.get(i)));
					advisors[i] = tmp;
				}
				eachNode.setParent(advisors);
			}

			// Set the children info for each node:
			List<Integer> cids = eachPer.getStudentIDs();
			if (cids != null && cids.size() > 0) {
				DAGNode[] children = new DAGNode[cids.size()];
				for (int i = 0; i < cids.size(); i++) {
					DAGNode tmp = this.tMap.get(cids.get(i));
					children[i] = tmp;
				}
				eachNode.setChildren(children);
			}

		}
	}

	// Get a node by its id from this DAG:
	public DAGNode getNode(int id) {
		return tMap.get(id);
	}

	private class PersonForLCACursor extends CursorWrapper {

		public PersonForLCACursor(Cursor c) {
			super(c);
		}

		/**
		 * Returns a Person object configured for the current row, or null if
		 * the current row is invalid.
		 */
		public PersonForLCA getPerson() {
			if (isBeforeFirst() || isAfterLast())
				return null;
			PersonForLCA person = new PersonForLCA(
					getInt(getColumnIndex(COLUMN_ID)));

			person.setName(getString(getColumnIndex(COLUMN_NAME)));

			int indexStudents = getColumnIndex(COLUMN_STUDENTS);
			String student = getString(indexStudents);
			if (student != null) {
				String[] studentsArray = student.split(",");
				List<Integer> students = new ArrayList<Integer>();

				for (String i : studentsArray) {
					students.add(Integer.parseInt(i));
				}

				person.setStudentIDs(students);
			}

			int indexParent = getColumnIndex(COLUMN_PARENTS);
			String parent = getString(indexParent);
			if (parent != null) {
				List<String> parents = Arrays.asList(parent.split(","));
				person.setAdvisorsIDs(parents);
			}

			return person;
		}
	}

}
