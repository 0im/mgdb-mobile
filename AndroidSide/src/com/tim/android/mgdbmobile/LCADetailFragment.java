package com.tim.android.mgdbmobile;

import java.util.ArrayList;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LCADetailFragment extends Fragment {
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_lca_detail, parent, false);

		LinearLayout names = (LinearLayout) v
				.findViewById(R.id.fragment_lca_detail_names);

		int i = 0;
		for (Person p : LCALab.get(getActivity()).getPeople()) {
			TextView name = new TextView(getActivity());
			name.setText(p.getName());
			name.setTypeface(null, Typeface.BOLD);
			name.setTextSize(14);
			name.setGravity(Gravity.CENTER);

			names.addView(name);

			i++;

			if (i != LCALab.get(getActivity()).size()) {
				TextView and = new TextView(getActivity());
				and.setText("And");
				and.setGravity(Gravity.CENTER);
				names.addView(and);
			}
		}

		TextView isare = new TextView(getActivity());
		isare.setText("is / are");
		isare.setGravity(Gravity.CENTER);
		names.addView(isare);
		
		ArrayList<Integer> result = LCALab.get(getActivity()).findLCA();

		return v;
	}

	private class FetchIDTask extends
			AsyncTask<String, Void, ArrayList<Person>> {

		@Override
		protected ArrayList<Person> doInBackground(String... keywords) {

			return new Parser().getLCAResult(Fetcher
						.getLCAId(LCALab.get(getActivity()).getIds()));

		}

		@Override
		protected void onPostExecute(ArrayList<Person> people) {
			
		}
	}
}
