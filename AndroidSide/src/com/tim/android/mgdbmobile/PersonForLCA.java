package com.tim.android.mgdbmobile;

import java.io.Serializable;
import java.util.List;

public class PersonForLCA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3312290322950539765L;
	int ID; // Person's id
	String name; // Person's name
	List<String> advisorsIDs; // Advisors Id
	List<Integer> studentIDs; // Student id
	

	public PersonForLCA(int id) {
		this.ID = id;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getID() {
		return ID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}


	public List<String> getAdvisorsIDs() {
		return advisorsIDs;
	}

	public void setAdvisorsIDs(List<String> advisorsIDs) {
		this.advisorsIDs = advisorsIDs;
	}

	public List<Integer> getStudentIDs() {
		return studentIDs;
	}

	public void setStudentIDs(List<Integer> studentIDs) {
		this.studentIDs = studentIDs;
	}

}
