package com.tim.android.mgdbmobile;

import java.io.Serializable;

public class Person implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7409824276123195702L;
	private String mId;
	private String mName;
	private String mUnverisity;
	private String mYear;
	private String mDissertation;
	private int mDescendants;
	
	
	public String getUnverisity() {
		return mUnverisity;
	}
	
	public void setUnverisity(String unverisity) {
		mUnverisity = unverisity;
	}
	
	public String getDissertation() {
		return mDissertation;
	}
	
	public void setDissertation(String dissertation) {
		mDissertation = dissertation;
	}
	
	public String getYear() {
		return mYear;
	}
	
	public void setYear(String year) {
		mYear = year;
	}
	
	public String getName() {
		return mName;
	}
	
	public void setName(String name) {
		mName = name;
	}
	
	public String getId() {
		return mId;
	}
	
	public void setId(String id) {
		mId = id;
	}

	public int getDescendants() {
		return mDescendants;
	}

	public void setDescendants(int descendants) {
		mDescendants = descendants;
	}

}
