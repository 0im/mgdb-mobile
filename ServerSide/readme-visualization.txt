The Visualization

-Requirements
-Setup
-Running
-Alterations


::Requirements
-PostgreSQL 
-Apache
-PHP
-R/Rscript
-R packages ( DBI, RPostgreSQL)
-VRML/X3D viewer ( http://www.bitmanagement.de/ )

::Setup

One thing that you will need to do is run your favorit force directed layout algorithem to get coordinates of each node for this we have used openord3d (code.google.com/p/openord3d) and included in the code our output file, it is a hair ball so if you'd like to use a different method this is where you can plug it in test.coord. 

Second Run the following query as a preprocessing step ( if table edges exist drop it same with the function):

create table edges as select author,advisor, 0 as count from advised a join dissertation d on ( a.student = d.did );

CREATE OR REPLACE FUNCTION getlongestpath() RETURNS SETOF edges AS
$BODY$
 DECLARE
      affected_rows   INTEGER DEFAULT 1;
BEGIN

update edges set count =1 where advisor in (
select author from dissertation  where did in (select did from dissertation except select student from advised));

    WHILE affected_rows > 0
    LOOP
      WITH updated_rows as (  UPDATE edges  
      SET count = subquery.max + 1
    FROM (
          select d.author,MAX(d.count) as max from edges d group by d.author having MIN(d.count) > 0
          ) AS subquery
          where edges.advisor = subquery.author AND count < (subquery.max + 1)
            RETURNING count
    )
       SELECT count(*) INTO affected_rows from updated_rows;
    END LOOP;
    RETURN QUERY (SELECT * from edges where count = (select max(count) from edges));
END
$BODY$
LANGUAGE 'plpgsql' ;

This will be all you need to do.

:: Running

Make sure lca.R is executable.

You would only need to run generator.R to draw the entire graph (this will output test.wrl which then you can open with the viewer, please note that this will be very intensive you need a good GPU  to render it all).

The android application will automatically call on lca.R to generate the subgraph, however you may also call it from a very poor webinterface with parent.html

For more information about the visualization please refer to the writeup.


:: Alterations

Due to the fact that BS contact was not built to iso standard we had to attempt several workarounds for displaying text in 3d, as it is now it is set to use texture. In the code we have commented out other methods of displaying this information the most efficent being quering it from the backend and rendering it as an image (see file detailimg.php) to use this simply Edit MGNode proto definition in lca.R and generator.R and uncomment the line //img.url = new... etc. Another method is to get the information generated as physical data and sent back via createvrmlfromurl this line also exists uncommenting it will get the information from the backend on demand. (this will still render them as horrific almost unreadable texture text if mobile can support text as is in the ISO spesifications you can remove the fontface texture from text nodes and replace them with a true font).