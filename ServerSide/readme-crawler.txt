 
The crawler

-Requirements
-Running
-The data
-Problems and Improvments


::Requirements
-R/Rscript base installation
-R packages( XML)

::Running

Make sure crawlerfinal.R is executable and simply run it from commandline, if there are problems with the interpretor edit the header to where your Rscript interpretor is stored. It will run untill it has collected as many records as stated in the MG website.

:: The data

The data is stored in several csv files in the following structure (in exact order):

 Create Table pstable (
dummy integer, --  unnessisary and meaningless
pid integer,       -- person id
name varchar(200), -- full name
fname varchar(150), -- first name
lname varchar(150), -- last name
mname varchar(150), -- middle name
onlinedescendants integer,
PRIMARY KEY(pid));

 create table dstable (
 dummy integer, -- garbage
did integer,  -- dissertation id
 author integer, -- the person id who wrote the dissertation
 title text,  
 year integer,  -- ONLY the first year is included here (if any)
university text,
country varchar(100), -- the same as the country flag name on the website
subject integer,  -- the subject which the dissertation is classified on the website
 PRIMARY KEY(did));

create table adtable (
advisororder integer, --order as they appear on the website (top down)
did integer, --dissertation id
sid integer, --student id
aid integer, -- advisor id
PRIMARY KEY(did,sid,aid)
);

::Problems and Improvements

The major problem with this is that it is slow, on the positive side it is very easy to modify and understand, however it takes about 25 hours on the conservative side to collect the majority of the data. A good improvement would be to change the stopping condition and how it attempts to recrawl failed records, as it is now it simply attaches the failed records to a list and reattmpts them each time it reaches the "end" id as well as increasing the end id based on the number of records on the website. Each time this end id is increased and the number of records aren't met the failed records will be retryed and thus slowing the process down significantly.

The minor problems are to do with bad data that is in MG website. For example douplicate enteries, multiple years for the same dissertation, html in person names, and most importantly inconsistent name ordering i.e. ( something something JR) in which case JR will be considered by the crawler as the last name , the first something the first name, and everything else the middle name.



