<?php 

class db {

  var $id;

  function db() { }
  
  function getq() { return $this->queries; }
  
  function open($database, $host=SQLITE3_OPEN_READONLY, $user="", $password="", $pconnect = 0) {
  
  try {
      $this->id = new PDO("sqlite:".$database);
     //$this->id = new SQLite3($database,$host);
  }
  catch (Exception $e) {
  $this->id=$pconnect; $this->error("<b>Fatal Error</b>:".$e); //$this->id->lastErrorMsg());
  }

    return $this->id;
  }

  function error($error = 0) {
    global $lastquery;
    print '<font face="verdana" size="5">UC Davis SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
    if (!$error) { $em= $this->error2(); print $em[2]; } else { print $error; }
	print '<br>Query:<br>'.$lastquery;
	print '</font>';
	exit();
  }
      function errorcheck() {
    global $lastquery;        if (intval($this->id->errorCode())) {
		print '<font face="verdana" size="5">UC Davis SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
		$em = $this->error2();
		print $em[2];
		print '<br>Query:<br>'.$lastquery;
		print '</font>';
		exit();	}
  }
	 
  function error2() {
    return $this->id->errorInfo();
  } 

  function escstring($string) {
	return SQLite3::escapeString($string);
  }
   
};

class query {

  var $result;
  var $row;

  function query(&$db, $query="",$prepare=0,$exec="") {
      global $lastquery;
	  $lastquery = $query;
    if(!$prepare){
     
      $this->result=$db->id->query($query);
    }
    else {
      $this->result=$db->id->prepare($query);
      $this->result->execute($exec);
    }
    
  /*   if (intval($this->result->errorCode())) {
		print '<font face="verdana" size="5">UC Davis Math Circle SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
		$em = $this->result->errorInfo();
		print $em[2];
		print '<br>Query:<br>'.$lastquery;
		print '</font>';
		exit();	}
*/
	  $db->errorcheck();
	  $db->queries .= '<br>'.$query;
  }
  
  function getrow() {
    if ($this->result) {
      $this->row=$this->result->fetch();
    } else {
      $this->row=0;
    }
    return $this->row;
  }
   
   
  function fetch() {
    if ($this->result) {
      $this->row=$this->result->fetch();
    } else {
      $this->row=0;
    }
    return $this->row;
  }   
  
  /* no longer valid for sqlite3
  function seek($row) {   
  
  return sqlite_seek($this->result, $row);  
  if ($row==0) {
    return sqlite_rewind($this->result);
  } else {
    return sqlite_seek($this->result, $row);
  }
 
  } */
   
  function field($field) {
	return $this->row[$field];
  }



}


?>