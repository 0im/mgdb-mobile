<?php 

class db {

  var $id;

  function db() { }
  
  function getq() { return $this->queries; }
  
  function open($database, $host="", $user="", $password="", $pconnect = 0) {
  
    if (!$pconnect=sqlite_open($database, 0666, $sqliteerror)) {
       $this->id=$pconnect; $this->error("<b>Fatal Error</b>:".$sqliteerror );
    } else {
       $this->id=$pconnect;
    }

    return $this->id;
  }

  function error($error = 0) {
    global $lastquery;
    print '<font face="verdana" size="5">UC Davis Math Circle SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
    if (!$error) { print $this->error2(); } else { print $error; }
	print '<br>Query:<br>'.$lastquery;
	print '</font>';
	exit();
  }
      function errorcheck() {
    global $lastquery;        if (sqlite_last_error($this->id)) {
		print '<font face="verdana" size="5">UC Davis Math Circle SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
		$em = $this->error2();
		print $em;
		print '<br>Query:<br>'.$lastquery;
		print '</font>';
		exit();	}
  }
	 
  function error2() {
    return sqlite_error_string(sqlite_last_error($this->id));
  } 
  
  function close() {
    $result=@sqlite_close($this->id);
    return $result;
  } 
};

class query {

  var $result;
  var $row;

  function query(&$db, $query="") {
      global $lastquery;
	  $lastquery = $query;
      $this->result=sqlite_query($db->id,$query);      $db->errorcheck();
	  $db->queries .= '<br>'.$query;
  }
  
  function getrow() {
    if ($this->result) {
      $this->row=sqlite_fetch_array($this->result, SQLITE_ASSOC);
    } else {
      $this->row=0;
    }
    return $this->row;
  }
   
   
  function fetch() {
    if ($this->result) {
      $this->row=sqlite_fetch_array($this->result, SQLITE_ASSOC);
    } else {
      $this->row=0;
    }
    return $this->row;
  }   
  
  function seek($row) {   
  
  return sqlite_seek($this->result, $row);  
  if ($row==0) {
    return sqlite_rewind($this->result);
  } else {
    return sqlite_seek($this->result, $row);
  }
  
  }
   
  function field($field) {
	return $this->row[$field];
  }



		
  function affected(&$db) {
		return @sqlite_changes($db->id);
	}

}


?>