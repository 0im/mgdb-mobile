<?php 

class db {

  var $id;
  var $dbtype;
  function db() { }
  
  function getq() { return $this->queries; }
  
  function open($itype, $database, $host, $user, $password, $pconnect = 0) {
  
    if (!($pconnect=dbx_connect($type,$host, $database, $user, $password))->handle) {

  $this->dbtype =$itype;
             
if (dbx_error()){ // if dbx does not contain the error message try to get it from native calls
$errmess = $this->error2();
}
else{
  $errmess = "Unknown!";
}


       $this->id=$pconnect; $this->error("<b>Fatal Error</b>:".$errmess );




    } else {
       $this->id=$pconnect;
    }

    return $this->id;
  }

  function error($error = 0) {
    global $lastquery;
    print '<font face="verdana" size="5">UC Davis Math Circle SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
    if (!$error) { print dbx_error($this->id); } else { print $error; }
	print '<br>Query:<br>'.$lastquery;
	print '</font>';
	exit();
  }
      function errorcheck() {
    global $lastquery;        if (dbx_error($this->id)) {
		print '<font face="verdana" size="5">UC Davis Math Circle SQL</font><br><br><font face="verdana" size="2">An Error has occured, please contact the system administrator.</font><br><br><br><hr><font face="verdana" size="1">';
		$em = dbx_error($this->id);
		print $em;
		/*$errbd = explode("'",$em); // no longer used 
		if ($errbd[3] == '. (errno: 145)'){
			$badtable = explode(".",$errbd[2]);
			mysql_query("REPAIR TABLE ".$badtable[0]);
		} */
		print '<br>Query:<br>'.$lastquery;
		print '</font>';
		exit();	}
  }
	 
  function error2() { //try to get old errors
switch ($this->dbtype) {
    case DBX_MYSQL:
        $errmess = mysql_error();
        break;
    case DBX_ODBC:
        $errmess = odbc_error();
        break;
    case DBX_PGSQL:
        $errmess = pg_last_error();
        break;
    case DBX_MSSQL:
        $errmess = mssql_get_last_message();
        break;
    case DBX_OCI8:
        $errmess = oci_error()['message'];
        break;
    case DBX_SQLITE:
        $errmess = sqlite_error_string (sqlite_last_error());
        break;
    
}

    return $errmess;
  } 
  
  function close() {
    $result=@dbx_close($this->id);
    return $result;
  } 
};

class query {

  var $result;
  var $row;
  var $pointer; //current position in record set

  function query(&$db, $query="") {
      global $lastquery;
	  $lastquery = $query;
      $this->result=dbx_query($db->id,$query);      $db->errorcheck();
	  $db->queries .= '<br>'.$query;
                  $this->pointer = 0;
  }
  
  function getrow() {
    if ($this->result && $this->pointer < $this->result->rows ) {
      $this->row= $this->result->data[$this->pointer];
      $this->pointer++;
    } else {
      $this->row=0;
    }
    return $this->row;
  }
   
   
  function fetch() {
    if ($this->result && $this->pointer < $this->result->rows ) {
      $this->row= $this->result->data[$this->pointer];
      $this->pointer++;
    } else {
      $this->row=0;
    }
    return $this->row;
  }   

// no longer in use
// function seek($row) {    return mysql_data_seek($this->result, $row);  }

function sort($func) {    return dbx_sort($this->result, $func);  }

  function field($field) {
	return $this->row[$field];
  }

// no longer in use
/*
  function free() {
    return @mysql_free_result($this->result);
  }

		
  function affected() {
		return @mysql_affected_rows();
	}
*/
};



?>
