The Mobile back end

-Requirements
-Setup


::Requirements
-PostgreSQL 
-Apache
-PHP
-PHP extentions: php_pg 

::Setup

Place the files in your httpd folder ( could be '/var/www/html/...' in linux) make sure apache has access to read them. 
Edit the file ./lib/sqldata.php to reflect your own database installation. If you would like to change to a database different than postgresql you must edit includes.php. We have included database abstractions for the following databses:

mysql: ./lib/db/mysql.php
oracle: ./lib/db/oracle.php
postgresql: ./lib/db/pgsql.php
sqlite3: ./lib/db/sqlite3.php
sqlite: ./lib/db/sqlite.php
mssql: ./lib/db/mssql.php 

NOTE** all of the above would require their own php extentions in order to work, further more mysql needs to be upgraded to mysqli but this is easy to do.

Changing the database might require reexamination of queries used in the php files but it will not be nessisary to change the structure of the code as long as the sql queries are valid in the new database.


