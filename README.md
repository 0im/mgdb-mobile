Installing MGDB-Mobile 
2.1 Back-End
1. Install Apache, PHP, and your favorite SQL (depending on your distribution the commands will 
        vary i.e. yum install httpd php5 mysql postgresql php_pg php_mysqli )
         2. Plug in the corresponding database abstraction class in workspace/lib/include.php file.
                 3. Edit sqldata.php with your host, database, username and password for opening connections.
                 4. Edit php.ini to enable database extensions if necessary and enable short tags.
         5. Create the database on your SQL server and import the data from the csv(s).
2.2 Front-End 
Download the /AndroidSide / bin / MGDBMobile.apk to your device.
Find the .apk file in the file manager and install it.
You may need to turn on “Unknown sources” in the settings.
